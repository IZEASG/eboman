# eboman

**eboman** is an ebook manager written in bash.

## Motivation

I wanted an ebook manager that was fast, straightforward and didn't use tons of dependencies. But also have the basic tools expected from an ebook manager.

## Dependencies

- ExifTool
- coreutils, sed and more
- E-book reader (Zathura, Okular, etc)

## Installation

Just copy or move it to your PATH.

## Usage

To list all the e-books in eboman, type:

```bash
eboman -l
```

![eboman list](./img/eboman_list.png "eboman list")

To filter the list:

```bash
eboman -F [ARG]
```

![eboman filter](./img/eboman_filter.png "eboman filter")

[ARG] can be a number or word

To add an e-book to eboman:

```bash
eboman -a path/to/file
```

To open an e-book:

```bash
eboman -o [ARG]
```

![eboman open](./img/eboman_open.png "eboman open")

To open the selected e-book, eboman uses xdg-open, so you still need a reader for the file you want to open.

To delete an e-book:

```bash
eboman -d [ARG]
```

To edit an e-book:

```bash
eboman -e [ARG]
```

[ARG] correspond to the number of the book you want to open, you can separate the numbers by commas (1,2,3) and/or a range (1-3).

If you want to edit the metadata of a book:

```bash
eboman -E [ARG]
```

After selecting the file you want to edit, you just need to uncomment the entries and write the changes you want to make.

![eboman meta](./img/eboman_meta.png)

Here [ARG] is a single number.

To refresh eboman after updating the metadata or manually adding an e-book to XDG_DATA_HOME/eboman:

```bash
eboman -u
```

And, to show the help message:

```bash
eboman -h
```
